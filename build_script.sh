#!/bin/sh
OPENAPI_CLI_VERSION=5.3.0
cd yaml-resolved
CHECK=$(curl -X POST --data-binary @swagger.yaml -H 'Content-Type:application/yaml' https://validator.swagger.io/validator/debug --silent)
if [[ $CHECK != "{}" ]]; then 
  echo -e "\nSorry this is an invalid Swagger:\n$CHECK\n"
  exit 1
fi;
#download openapicli latest jar from mvn repository
cd ..
curl https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/$OPENAPI_CLI_VERSION/openapi-generator-cli-$OPENAPI_CLI_VERSION.jar --silent --output openapi-generator-cli.jar
java -jar ./openapi-generator-cli.jar generate -g spring -i ./yaml-resolved/swagger.yaml -c config.json -o spring-boot-codegenerator
cd spring-boot-codegenerator
mvn clean install
echo "Build completed"
if [[ $CI == true ]]; then
    swagger_version=$(ls target/*.jar | sed "s/^.*openapi-spring-\([0-9.]*\).*/\1/")
    jar_file=$(ls target/*.jar)
    #upload artifact to jfrog
    echo $jar_file
    cd ..
    curl -X PUT -u $JFROG_USER:$JFROG_PASS -T spring-boot-codegenerator/$jar_file "https://chanduartifact.jfrog.io/artifactory/sample/openapi-spring-${swagger_version}.jar"
    curl -X PUT -u $JFROG_USER:$JFROG_PASS -T yaml-resolved/swagger.yaml "https://chanduartifact.jfrog.io/artifactory/sample/swagger-${swagger_version}yaml"
fi
